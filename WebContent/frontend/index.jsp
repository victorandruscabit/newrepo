<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../css/style.css">
<title>Book Store Website</title>


</head>
<body>

	<jsp:directive.include file="header.jsp" />

	<div class="center">
		<div>
			<h1>
				<b>New books : </b>
			</h1>
			<c:forEach items="${listNewBooks}" var="book">
				<div class="book">
					<div>
						<a href="view_book?id=${book.bookId}"> <img class="book_small"
							src="data:image/jpg;base64,${book.base64Image}" />

						</a>
					</div>

					<div>
						<a href="view_book?id=${book.bookId}"> <b>${book.title}</b>
						</a>
					</div>

					<div>
						<jsp:directive.include file="book_rating.jsp"/>
					</div>
					<div>
						<i>by ${book.author}</i>
					</div>
					<div>
						<b></b> $${book.price}</b>
					</div>
				</div>

			</c:forEach>
		</div>

		<div class="next_row">
		
						<c:forEach items="${listBestSellingBooks}" var="book">
				<div class="book">
					<div>
						<a href="view_book?id=${book.bookId}"> <img class="book_small"
							src="data:image/jpg;base64,${book.base64Image}" />

						</a>
					</div>

					<div>
						<a href="view_book?id=${book.bookId}"> <b>${book.title}</b>
						</a>
					</div>

					<div>
						<jsp:directive.include file="book_rating.jsp"/>
					</div>
					<div>
						<i>by ${book.author}</i>
					</div>
					<div>
						<b></b> $${book.price}</b>
					</div>
				</div>

			</c:forEach>
			
		</div>
		<div class="next_row">
			<h2>Most-favored Books :</h2>
			<c:forEach items="${listFavoredBooks}" var="book">
				<div class="book">
					<div>
						<a href="view_book?id=${book.bookId}"> <img class="book_small"
							src="data:image/jpg;base64,${book.base64Image}" />

						</a>
					</div>

					<div>
						<a href="view_book?id=${book.bookId}"> <b>${book.title}</b>
						</a>
					</div>

					<div>
						<jsp:directive.include file="book_rating.jsp"/>
					</div>
					<div>
						<i>by ${book.author}</i>
					</div>
					<div>
						<b></b> $${book.price}</b>
					</div>
				</div>

			</c:forEach>
		</div>
		<br /> <br />
	</div>


	<jsp:directive.include file="footer.jsp" />
</body>
</html>